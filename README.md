# APILESSYT

With this module you can interact with youtube, without the use of the official Youtube API.

# But why would you do that?
Google APIs are unnecessarily complex and often come with limitations, such as no public Videos until Google reviewed your app, or a limitation in the number of comments.

# How does it work?
Instead of using the api, we simply use [puppeteer](https://github.com/puppeteer/puppeteer), a headless browser, to upload it via the youtube webpage.

# Important notice
By using this package you are violating the YouTube TOS and you may get banned, I do not take responisibilty for any bans that occur by using this package. However the risk of a ban is slim to none and so far no banned accounts have been reported, even when acting suspiciously during testing.

Also there may appear some bugs if your internet is to slow, those will get fixed in future updates.
 
# Usage
## Requireing and methods
Simply require the module with

```
const yt = require('apilessyt')
```

before using any methods you must login to your youtube account.

```
await yt.login({
    name: GOOGLE_EMAIL_OR_PHONE,
    password: GOOGLE_PASSWORD,
    twofa: false //set this to true if you are using 2FA to login to Google
})
```

after you are logged in you can use any methods curently available. Your caches should get saved so you only have to login once.


to upload a video just call the upload function with the given specs.

```
await yt.upload(specs)
```

You can also clear your cache by executing

```
await yt.reset();
```

To toggle the pupeteer visibility use

```
yt.setHead(boolean);
```

To increase (or decrease) the delay between button presses

```
setDefaultDelayMultiplier(number)
```

Additional features like, writing comments, etc. may follow in the future

## Specs
The specs are simply an Object with the desired options.

### Upload Specs
These are the specs with all current available functions. They get parsed and check beforehand, this method is not thoroughly yet though. If the bot crashes because of bad specs, you are left with a draft on your youtube account.
```
{
    channelId: string;                      // Your Channel ID
    video: string;                          // Filepath to the video
    pageOne: {
        title: string;
        description?: string;
        thumb?: string;                      // Filepath to the Thumbnail
        playlists?: string[];                // Write the name of the (existing)playlists you want to add your video to as a string array
        madeForKids?: boolean;
        restrictForAdultsOnly?: boolean;
        paidPromotion?: boolean;
        allowAutomaticChapters?: boolean;
        tags?: string[];                    // Write any tags as an array of strings
    };
    pageTwo?: {
        endcard?: {
            enabled: boolean;
            import: boolean;                // If the endcard should be imported from another video
            importId?: string;              // Defines the videoId of the video where the endcard will be imported from (you can also use the video name)
            type?: endcard;                 // Type of the endcard, see types, only needs to be set if you dont import
        };
        cards?: {
            enabled: boolean;
            type: card;                     // See types
            name: string;                   // name or (reccomended)id of the video/playlist/channel,
            message?: string;
            teaser?: string;
            anyVideo?: boolean;             // Only search your own Videos, or search for any Video on YouTube
        };
    };
    pageThree?: {};
    pageFour: {
        visibility: visibility;             // See types
        instantPremiere?: boolean;
    };
    creatorComment?: {                      // You can also add an creator comment to your uploaded video
        text: string;
        pin?: boolean;
    };
}
```

## Types
You can import the enums for visibility, cards, etc. with this line.
```
const types = yt.types;
```

```
enum endcard {
    T1 = 1,
    T2 = 2,
    T3 = 3,
    T4 = 4,
    T5 = 5,
    T6 = 6,
}

enum card {
    VIDEO = 2,
    PLAYLIST = 3,
    CHANNEL = 4,
    LINK = 5,
}

enum visibility {
    PRIVATE = "PRIVATE",
    UNLISTED = "UNLISTED",
    PUBLIC = "PUBLIC",
}
```

## Additional Information
This is an very early version, some features may be buggy, especially with slow internet.


# Future Features

- Any setting that is available when uploading to youtube
- Additional features like returning the upload link/other data
- Wrting comments on different videos

(Please excuse the messy code, i will clean up in the future 👉👈)